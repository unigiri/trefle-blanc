module.exports = {
  purge: {
    options: {
      safelist: [/^hover:bg-/, /^border-/],
    },
  },
}
