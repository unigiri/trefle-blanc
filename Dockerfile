FROM node:16-alpine3.15

WORKDIR /app
COPY package*.json .

RUN apk update && \
    apk upgrade && \
    apk add git
RUN yarn install

EXPOSE 3000
ENV HOST 0.0.0.0
